import tkinter as tk
import re

# validar el correo electrónico


def validar_correo(correo):
    patron = r'^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$'
    return re.match(patron, correo)

# validar la contraseña


def validar_contraseña(contraseña):
    patron = r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{12,}$'
    return re.match(patron, contraseña)

# verificar los datos "email, contraseña y la confir"


def verificar():
    correo = correo_input.get()
    contraseña = contraseña_input.get()
    confirmar_contraseña = confirmar_contraseña_input.get()

    if not validar_correo(correo):
        resultado_label.config(text="Correo inválido", fg="red")
        return

    if not validar_contraseña(contraseña):
        resultado_label.config(text="Contraseña inválida", fg="red")
        return

    if contraseña != confirmar_contraseña:
        resultado_label.config(text="Las contraseñas no coinciden", fg="red")
        return

    # si los datos todos son iguales se abre la ventana para conffirmasr.
    ventana_loggeado()


def ventana_loggeado():
    ventana = tk.Toplevel(root)
    ventana.title("Loggeado")
    ventana.geometry("100x100")
    tk.Label(ventana, text="ESTÁS LOGGEADO", fg="green",
             font=("Arial", 16, "bold")).pack(pady=20)


# Crear ventana principal
root = tk.Tk()
root.title("Alta de Usuarios")
root.geometry("500x430")

# Aviso
label_aviso = tk.Label(root, text="Porfavor ingrese su contraseña y correo electrónico", font=(
    "Arial", 14), bg="#3174B8", fg="white")

# Titulo
titulo_label = tk.Label(root, text="Alta de Usuarios",
                        font=("Arial", 14, "bold"))

# Sección de Correo
correo_label = tk.Label(root, text="Correo electrónico", font=("Arial", 14))
correo_input = tk.Entry(root, font=("Arial", 14))

# Sección de Contraseña
contraseña_label = tk.Label(root, text="Contraseña", font=("Arial", 14))
contraseña_input = tk.Entry(root, show="*", font=("Arial", 14))

# Sección de Confirmar Contraseña
confirmar_contraseña_label = tk.Label(
    root, text="Confirmar Contraseña", font=("Arial", 14))
confirmar_contraseña_input = tk.Entry(root, show="*", font=("Arial", 14))

# Sección de Botón
verificar_btn = tk.Button(root, text="Verificar",
                          font=("Arial", 18), command=verificar)

# Sección donde muestra los errores
resultado_label = tk.Label(root, text="", font=("Arial", 14))

# Estructura y acomodados
label_aviso.pack(pady=10)
titulo_label.pack(pady=10)
correo_label.pack()
correo_input.pack(pady=10)
contraseña_label.pack()
contraseña_input.pack(pady=10)
confirmar_contraseña_label.pack()
confirmar_contraseña_input.pack(pady=10)
verificar_btn.pack(pady=20)
resultado_label.pack()

# mainloop
root.mainloop()
